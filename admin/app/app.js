// Firebase -->
// ***********************************************************************************************************************
var config = {
    apiKey: "AIzaSyCfSNSONM7XsijA-xWLB_jMosYSFpubXgo",
    authDomain: "jazz-c8ba1.firebaseapp.com",
    databaseURL: "https://jazz-c8ba1.firebaseio.com",
    projectId: "jazz-c8ba1",
    storageBucket: "jazz-c8ba1.appspot.com",
    messagingSenderId: "1074998454805"
};
firebase.initializeApp(config);
//   * TODO(DEVELOPER): Paste the initialization snippet from: Firebase Console > Overview > Add Firebase to your web app. *
//   *********************************************************************************************************************** 
var app = angular.module('app', ['ezfb','ui.router','uiSwitch','oitozero.ngSweetAlert','angularFileUpload','ui.sortable','ui-notification','angularjs-datetime-picker','textAngular','ui.bootstrap']);
app.config(['$stateProvider','$urlRouterProvider','$locationProvider','$httpProvider','NotificationProvider', '$httpProvider' ,'ezfbProvider',
	function ($stateProvider,$urlRouterProvider,$locationProvider,$httpProvider,NotificationProvider, $httpProvider, ezfbProvider) {
	ezfbProvider.setInitParams({
		// This is my FB app id for plunker demo app
		appId: '1572281729735587',

		// Module default is `v2.6`.
		// If you want to use Facebook platform `v2.3`, you'll have to add the following parameter.
		// https://developers.facebook.com/docs/javascript/reference/FB.init
		version: 'v2.3'
	});  
	NotificationProvider.setOptions({
        delay: 3000,
        startTop: 20,
        startRight: 10,
        verticalSpacing: 20,
        horizontalSpacing: 20,
        positionX: 'right',
        positionY: 'top'
    });
	$urlRouterProvider.otherwise('/');
	// $locationProvider.html5Mode(true);
	$stateProvider
	.state('main',{
		url : '/',
		templateUrl : 'app/views/includes/main.html',
		controller: 'MainCtrl'
	})
	.state('profile',{
		url : '/profile',
		templateUrl : 'app/views/profile/index.html',
		controller: 'ProfileCtrl'
	})
	.state('comments',{
		url : '/comments',
		templateUrl : 'app/views/comments/index.html',
		controller: 'CommentsCtrl'
	})
	.state('setting',{
		url : '/setting',
		templateUrl : 'app/views/setting/index.html',
		controller: 'SettingCtrl'
	})	
}]);

var redirectToLogin = function () {
    window.location.href = "login.html";
};


app.run(['$rootScope', '$location',function ($rootScope,$location) {
    firebase.auth().onAuthStateChanged(function(user) {
      var keygen = sessionStorage.getItem('keygen')
	  if (keygen) {
	  	if (keygen !== user.uid) {
	  		redirectToLogin()
	  	}
	  }else{
	  	redirectToLogin()
	  }
	});    
}]);

