app.factory('EventService', ['$http','Basepath',function ($http,Basepath) {
	return {
		getEvent : function(){
			return $http.get(Basepath+'event');
		},
		getEventList : function(id){
			return $http.get(Basepath+'event/list/' + id);
		},
		getEventListByid: function(id){
			return $http.get(Basepath+'event/listByid/' + id);
		},
		CreateEvent : function(data){
			return $http({
				url: Basepath + 'event/create',
				method: "POST",
				data: $.param(data),
				headers:{
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			});
		},
		ActiveEvent: function(active , id){
			return $http.get(Basepath+'event/active/'+active+'/'+id);
		},
		ActiveListEvent: function(active , id){
			return $http.get(Basepath+'event/list/active/'+active+'/'+id);
		},
		DeleteEvent: function(id){
			return $http.get(Basepath+'event/'+id);
		},
		CreateList : (data) => {
			return $http({
				url: Basepath + 'event/list',
				method: "POST",
				data: $.param(data),
				headers:{
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			});
		},
		DeleteList: function(id){
			return $http.get(Basepath+'event/deletelist/'+id);
		},
		UpdateList: (data) => {
			return $http({
				url: Basepath + 'event/updateList',
				method: "POST",
				data: $.param(data),
				headers:{
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			});
		},
		UpdateEventOrder : (data) => {
			return $http({
				url: Basepath + 'event/update/order',
				method: "POST",
				data: $.param(data),
				headers:{
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			});
		},
		UpdateEventListOrder : (data) => {
			return $http({
				url: Basepath + 'event/list/update/order',
				method: "POST",
				data: $.param(data),
				headers:{
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			});


		}

	};
}]);
