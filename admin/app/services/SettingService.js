app.factory('SettingService', ['$http','Basepath',function ($http,Basepath) {
	return {
		getSetting : function(){
			return $http.get(Basepath+'setting')
		},
		UpdateSetting: (data) =>{
			return $http({
				url: Basepath + 'setting/update',
				method: "POST",
				data: $.param(data),
				headers:{
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			});
		}
	};
}]);