app.factory('AnalyticsService', ['$http','Basepath',function ($http,Basepath) {
	return {
		getAnalytics : function(){
			return $http.get(Basepath+'analytics');
		},
		getDevice : function(){
			return $http.get(Basepath+'analytics/getdevice');
		},
		getBrowser : function(){
			return $http.get(Basepath+'analytics/getbrowser');
		},
		getCountry : function(){
			return $http.get(Basepath+'analytics/getcountry');
		}
	};
}]);
