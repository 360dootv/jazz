app.controller('ResultsCtrl',['$scope','$rootScope', '$location','$state','SweetAlert','$stateParams','Notification',
	function ($scope,$rootScope,$location,$state,SweetAlert,$stateParams,Notification){
	$rootScope.title = 'Admin | Questions Zone';

	var questions = firebase.database().ref('questions');
	var user = firebase.database().ref('user');
	var answerTrue = firebase.database().ref('answertrue');


  	/*Show Live Event */
  	$scope.preload = true
  	user.once('value',function(snapshots){
		if (snapshots.val() !== null) {
			$scope.preload = false
			$scope.Questions_valid = false

			$scope.user = []
			snapshots.forEach(function(s){
				var val = {
					key:s.key, 
					objectID:s.val().objectID,
					Name:s.val().Name,
					LastName: s.val().LastName,
					TableNo: s.val().TableNo,
					activated: s.val().activated,
					email: s.val().email,
					Group : s.val().Group,
					answers : s.val().answers ? s.val().answers : null,
					accessTotal: s.val().accessTotal, 
				}
				$scope.user.push(val);
			})
			// length UserActivated
			var UserActivated = _.filter($scope.user, { activated: true })
			var groupedItems = _(UserActivated).groupBy(item => item.TableNo).value()
		    var result = _.map(groupedItems, function (info) {
		    	var table = info[0].TableNo
		    	var ScoreGetMutiply = HandleScoreGetMutiply(HandleScore(info), parseInt(table))
		    	if ((table >= 1) && (table <= 50)) {
		        	var val = {
		          		TableNo: parseInt(table),
		          		DataScore :  HandleScore(info),
		          		multiply: ScoreGetMutiply.multiply.toFixed(2),
		          		Count: ScoreGetMutiply.Count,
		          		Score: parseInt(HandleScoreMutiply(HandleScore(info), parseInt(table)).toFixed(2))
		        	}
		        	return val
		    	}
		    })	

		    var mapWithout  = _(result).omitBy(_.isUndefined).value()
		    $scope.mapResults = _.orderBy(mapWithout, ['Score'], ['desc'])
		    console.log($scope.mapResults)

			function HandleScore (HandleScore) {
				var win = 0
				var lost = 0
				angular.forEach(HandleScore, function(info){
					angular.forEach(info.answers, function(answers){
						if (answers.result === true) {
							win++
						} else if (answers.result === false){
							lost++
						}
					})
				})
				return win
			}	

			function HandleScoreGetMutiply (HandleScore, table) {
				var resultMulti = _.map(groupedItems, function (info) {
			    	var table = info[0].TableNo
			    	if ((table >= 1) && (table <= 50)) {
			        	var val = {
			          		TableNo: parseInt(table),
			          		Count: info.length,
			          		multiply: 10 / info.length
			        	}
			        	return val
			    	}
			    })
			    var multiResult  = _(resultMulti).omitBy(_.isUndefined).value()
			    var multiplyData = _.filter(multiResult, { 'TableNo': table });
			    return multiplyData[0]
			}	

			function HandleScoreMutiply (HandleScore, table) {
				var resultMulti = _.map(groupedItems, function (info) {
			    	var table = info[0].TableNo
			    	if ((table >= 1) && (table <= 50)) {
			        	var val = {
			          		TableNo: parseInt(table),
			          		Count: info.length,
			          		multiply: 10 / info.length
			        	}
			        	return val
			    	}
			    })
			    var multiResult  = _(resultMulti).omitBy(_.isUndefined).value()
			    var multiplyData = _.filter(multiResult, { 'TableNo': table });
			    return multiplyData[0].multiply * HandleScore
			}	

			if (!$scope.$$phase) {
				$scope.$apply();
			}
		}else if (snapshots.val() === null){
			$scope.preload = false;
			$scope.Questions_valid = true;
			if (!$scope.$$phase) {
				$scope.$apply();
			}
		}
	})


	user.once('value',function(snapshots){
		if (snapshots.val() !== null) {
			$scope.user = []
			snapshots.forEach(function(s){
				var val = {
					key:s.key, 
					objectID:s.val().objectID,
					Name:s.val().Name,
					LastName: s.val().LastName,
					TableNo: s.val().TableNo,
					activated: s.val().activated,
					email: s.val().email,
					Group : s.val().Group,
					answers : s.val().answers ? s.val().answers : null,
					accessTotal: s.val().accessTotal, 
				}
				$scope.user.push(val);
			})

			$scope.userComments = []
			angular.forEach($scope.user, function(info){
				if (info.answers !== null) {
					angular.forEach(info.answers, function(snaps){
						if (snaps.type === 'comments') {
							var val = {
								key:info.key, 
								objectID:info.objectID,
								Name:info.Name,
								LastName: info.LastName,
								TableNo: info.TableNo,
								email: info.email,
								text: snaps.text,
								type: snaps.type
							}
							$scope.userComments.push(val)
						}
					})
				}
			})
		}
	})

  	questions.once('value', function(snapshots){
  		$scope.quiz = []
  		$scope.quizTrue = []
  		snapshots.forEach(function(s){
  			if (s.val().type === 'choice' && s.val().calculate === false) {
  				$scope.quiz.push(s.val())
  			}

  			if (s.val().type === 'choice' && s.val().calculate === true) {
  				$scope.quizTrue.push(s.val())
  			}
  		})
  		
  		console.log($scope.quizTrue)

  		if (!$scope.$$phase) {
			$scope.$apply();
		}
  	})

  	$scope.CountDataNotCal = function(id){
  		var count = 0
  		var AllCount = 0
  		angular.forEach($scope.user, function(data){
  			if (data.answers !== null) {
  				var objData = _.filter(data.answers, { 'id': id })
  				if (objData.length === 1) {
  					count++
  				}
			}
  		})
  		return count 
  	}

  	$scope.CountDataCal = function(id){
  		var count = 0
  		var AllCount = 0
  		angular.forEach($scope.user, function(data){
  			if (data.answers !== null) {
  				var objData = _.filter(data.answers, { 'id': id })
  				if (objData.length === 1) {
  					count++
  				}
			}
  		})
  		return count 
  	}

}]);
