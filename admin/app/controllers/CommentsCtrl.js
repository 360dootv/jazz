app.controller('CommentsCtrl',['$http','$scope','$rootScope', '$location','$state','SweetAlert','$stateParams','Notification', 'ezfb','$interval',
	function ($http,$scope,$rootScope,$location,$state,SweetAlert,$stateParams,Notification, ezfb,$interval ){
	$rootScope.title = 'Admin | Questions Zone';

	var comments = firebase.database().ref('comments');
	var settings = firebase.database().ref('settings');
	var commentspin = firebase.database().ref('commentspin');
	$scope.statusLive = false;

	/*Show Live Event */
	settings.on('value',function(snapshots){
		$scope.settings = snapshots.val()
		if (!$scope.$$phase) {
			$scope.$apply();
		}
	})

	comments.on('value',function(snapshots){
		$scope.comments = snapshots.val()
		if (!$scope.$$phase) {
			$scope.$apply();
		}
	})

    commentspin.on('value', function(snapshot){
        $scope.commentspin = snapshot.val();
        if (!$scope.$$phase) { $scope.$apply(); }
    })

  	// activeCallback
	$scope.activeCallback = function(data) {
		var key = data.key;
		var data_questions = {
			status : data.status 
		}
		var questions_active = firebase.database().ref('questions/'+key);
		questions_active.update(data_questions);
		if (data.status) {
			angular.forEach($scope.datas, function(s){
				if (s.key == key) {
					Notification.success('Active Questions Success !!!');
					console.log(s.key);
				}else{
					var data_active = {
						status : false 
					}
					firebase.database().ref('questions/'+s.key).update(data_active);
				}
			})
		}
	};

	// DeleteQuestions
	$scope.UpdateLive = function(data) {
		settings.update({
			idLive: data.id,
			urlLive: data.url
		})
		Notification.success('Active Success !!!');
	};

	$scope.UpdateComment = function(data) {
		var comments = firebase.database().ref('comments');
		comments.update({
			comment: data.message,
			image: 'http://graph.facebook.com/'+data.from.id+'/picture?type=normal',
			id: data.from.id,
			name: data.from.name,
		 idComment: data.id
		})
		Notification.success('Active Success !!!');
	};

	$scope.UpdatePin = function(data) {
		var commentspin = firebase.database().ref('commentspin');
		commentspin.update({
			comment: data.comment,
		})
		Notification.success('Active Success !!!');
	};

	$scope.live = {
		link: '',
		access_token: ''
	}

	$scope.SyncLive = function(data) {
		Notification.success('Active Success !!!');
		function get () {	
		  $http.get('https://graph.facebook.com/v2.9/'+$scope.live.link.split('/')[5]+'/comments?limit=8&order=reverse_chronological&access_token='+$scope.live.access_token)
		  .then(function(res){
		  	$scope.dataComments = res.data.data
		  });
		}
		get ()
		$interval(function() {
		    get ()
		}, 15000);
	};
	




	
}]);
