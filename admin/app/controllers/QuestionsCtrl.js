app.controller('ProfileCtrl',['$scope','$rootScope', '$location','$state','SweetAlert','$stateParams','Notification',
	function ($scope,$rootScope,$location,$state,SweetAlert,$stateParams,Notification){
	$rootScope.title = 'Admin | Questions Zone';

	var questions = firebase.database().ref('questions');
	var settings = firebase.database().ref('settings');


	/*Show Live Event */
	settings.on('value',function(snapshots){
		$scope.settings = snapshots.val()
		if (!$scope.$$phase) {
			$scope.$apply();
		}
	})
  	/*Show Live Event */
	  	$scope.preload = true;
	  	questions.on('value',function(snapshots){
			if (snapshots.val() !== null) {
				$scope.preload = false;
				$scope.Questions_valid = false;

				$scope.datas = [];
				snapshots.forEach(function(s){
					var val = {
						key:s.key, 
						objectID:s.val().objectID, 
						subject:s.val().subject, 
						question: s.val().question,
						calculate: s.val().calculate, 
						timer: s.val().timer,
						type: s.val().type,
						status : s.val().status, 
						answer: s.val().answer, 
					}
					$scope.datas.push(val);
				})
				console.log($scope.datas)
				if (!$scope.$$phase) {
					$scope.$apply();
				}
			}else if (snapshots.val() === null){
				$scope.preload = false;
				$scope.Questions_valid = true;
				if (!$scope.$$phase) {
					$scope.$apply();
				}
			}
		})


	  	// activeCallback
		$scope.StartGame = function(data) {
			console.log(data)
			var setting = {
				quizStart: data.quizStart 
			}
			var questions_active = firebase.database().ref('settings');
			questions_active.update(setting);
		};
	  	// activeCallback
		$scope.activeCallback = function(data) {
			var key = data.key;
			var data_questions = {
				status : data.status 
			}
			var questions_active = firebase.database().ref('questions/'+key);
			questions_active.update(data_questions);
			if (data.status) {
				angular.forEach($scope.datas, function(s){
					if (s.key == key) {
						Notification.success('Active Questions Success !!!');
						console.log(s.key);
					}else{
						var data_active = {
							status : false 
						}
						firebase.database().ref('questions/'+s.key).update(data_active);
					}
				})
			}
		};

		// DeleteQuestions
		$scope.DeleteQuestions = function(data) {
			SweetAlert.swal({
			   title: "Are you sure?",
			   text: "Your will not be able to recover this record!",
			   type: "warning",
			   showCancelButton: true,
			   confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
			   cancelButtonText: "cancel",
			   closeOnConfirm: false}, 
			function(isConfirm){ 
			   if (isConfirm) {
				var questionsData = firebase.database().ref('questions/'+data.key);
				questionsData.remove(function(err){
					if (err) {
						SweetAlert.swal("Error !!!");
					}else{
						Notification.success('Deleted Questions Success !!!');
		        		SweetAlert.swal("Deleted!", "Your record has been deleted.", "success");
		        		$state.go('questions')
					}
				});
			   }
			});
		};

	/*Show Live Event */
}]);
