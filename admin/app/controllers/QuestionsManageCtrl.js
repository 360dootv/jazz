app.controller('QuestionsManageCtrl',['$scope','$rootScope', '$location','$state','SweetAlert','$stateParams','Notification',
    function ($scope,$rootScope,$location,$state,SweetAlert,$stateParams,Notification){

	$rootScope.title = 'Admin | Manage Questions';
	$scope.selectSM = ['choice' , 'comments'];
    // $scope.selectSM = ['choice'];
    var key_  = $stateParams.key;
    var questions = firebase.database().ref('questions/'+key_);

    $scope.UpdateQuestions = function(data){
        if (data.AnswerTrue !== '') {
            var questions = firebase.database().ref('questions/'+data.key);
            var info = {
                key:data.key, 
                objectID:data.objectID, 
                subject:data.subject, 
                question: data.question,
                calculate: data.calculate, 
                timer: data.timer,
                status : false, 
                type : data.type, 
                answers: data.answers, 
                correct: data.AnswerTrue
            }

            questions.update(angular.fromJson(angular.toJson(info)) , function(err){

                if (err) {
                    SweetAlert.swal("Error Update!!!");
                }else{
                    setTimeout(function(){
                        $state.go('questions')
                    },1000)
                    SweetAlert.swal("Good job!", "Update " + data.subject + " Success!", "success");
                    // var AnswerObjectID = firebase.database().ref('answertrue/'+data.objectID);
                    // AnswerObjectID.set(angular.fromJson(angular.toJson(data.AnswerTrue)) , function(err){
                    //     if (err) {
                    //         SweetAlert.swal("Error ADD Answer True !!!");
                    //     }

                    // });
                }
            });
        }
    }


    $scope.CreateQuestions = function(data){
        if (data.AnswerTrue !== 'undefined') {
            var questions = firebase.database().ref('questions');
            var info = {
                key:data.key, 
                objectID:data.objectID, 
                subject:data.subject, 
                question: data.question,
                calculate: data.calculate, 
                timer: data.timer,
                status : false, 
                type : data.type, 
                answers: data.answers, 
                correct: data.AnswerTrue
            }

            questions.push(angular.fromJson(angular.toJson(info)) , function(err){
                if (err) {
                    SweetAlert.swal("Error Created!!!");
                }else{
                    SweetAlert.swal("Good job!", "Created " + data.subject + " Success!", "success");
                    $state.go('questions')
                }
            });
        }
    }


    $scope.RemoveAnswer = function(data){
        data.answers.splice(-1,1)
    }

    $scope.AddAnswer = function(data){
        var info = {
            id : generate(),
            text: "DEMO ANSWER"
        }
        data.answers.push(info)
    }

    const generate = () => {
      var stringArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '!', '?']
      var rndString = ''
      var stringLength = 20
      for (var i = 1; i < stringLength; i++) {
        var rndNum = Math.ceil(Math.random() * stringArray.length) - 1
        rndString = rndString + stringArray[rndNum]
      }

      return rndString
    }
    if (key_) {
        questions.on('value',function(snapshots){
            if (snapshots.val() !== null) {
                $scope.info = {
                    key:snapshots.key, 
                    objectID:snapshots.val().objectID, 
                    subject:snapshots.val().subject, 
                    question: snapshots.val().question,
                    calculate: snapshots.val().calculate, 
                    timer: snapshots.val().timer,
                    status : false, 
                    type : snapshots.val().type, 
                    answers: snapshots.val().answers, 
                    correctId: snapshots.val().correct.id
                }
                $scope.AnsIDTrue = $scope.info.correctId 

                if (!$scope.$$phase) {
                    $scope.$apply();
                }
            }
            
        })
    }else{
        $scope.info = {
             objectID: generate(),
             status: false,
             calculate: false,
             timer: 60,
             subject: 'QUIZ SESSION',
             question: 'QUESTION DEMO ?',
             answers: [{
                     id: generate(), 
                     text: 'DEMO ANSWER',
                },{
                     id: generate(), 
                     text: 'DEMO ANSWER',
                }
            ]
        }
    }

}]);
