app.controller('MainCtrl',['$scope','$rootScope', '$location','$state','SweetAlert','$stateParams','Notification',
	function ($scope,$rootScope,$location,$state,SweetAlert,$stateParams,Notification){
// *******************  variable   *******************  
	var user = firebase.database().ref('user');
	$scope.realtimeUser = true

// *******************  variable   *******************  
function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  	if(month.toString().length == 1) {
        var month = '0'+month;
    }
    if(date.toString().length == 1) {
        var date = '0'+date;
    }   
    if(hour.toString().length == 1) {
        var hour = '0'+hour;
    }
    if(min.toString().length == 1) {
        var min = '0'+min;
    }
    if(sec.toString().length == 1) {
        var sec = '0'+sec;
    }   
  var time = date + '-' + month + '-'+ year+' '+ hour + ':' + min + ':' + sec ;
  return time;
}

// ******************* end  get online   *******************
 //    Notification.error('Data in Process......');
	// setTimeout(function(){
	// 	var user = firebase.database().ref('user');
	// 	user.once('value',function(snapshots){
	// 		if (snapshots.val() !== null) {
	// 			console.log(snapshots.val())
	// 			$scope.users = [];
	// 			var count = 1;
	// 			snapshots.forEach(function (s){
	// 				$scope.users.push([
	// 					count, 
	// 					s.val().uid,
	// 					`<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
	// 					  Launch demo modal
	// 					</button>`,
	// 					'<img src="'+s.val().photoURL+'" width="150px">',
	// 					s.val().displayName, 
	// 					s.val().email,
	// 					s.val().address,
	// 					s.val().phone,
	// 					s.val().lastAccess ? timeConverter(s.val().lastAccess) : ''
	// 				]);
	// 				count++;
	// 			})

	// 			$(document).ready(function(){
	// 				var table = $('#userlog').DataTable( {
	// 			        data: $scope.users.reverse(),
	// 			        "bDestroy": false,
	// 			        "ordering": true,
	// 			        "autoWidth": true,
	// 			        columns: [
	// 			        	{ title: "No." },
	// 			        	{ title: "Key ID" },
	// 			        	{ title: "Active" },
	// 			        	{ title: "Picture" },
	// 			        	{ title: "Display Name" },
	// 			            { title: "Email" },
	// 			            { title: "Address" },
	// 			            { title: "Phone" },
	// 			            { title: "Last Access" },
	// 			        ]
	// 			    });
	// 			});

	// 			Notification.success('Data is Success');
	// 			if (!$scope.$$phase) { $scope.$apply(); }
				
	// 		}else{
	// 			console.log('data null')
	// 		}		
	// 	})
	// },2000)

	$scope.realtimeUsers = function () {
		location.reload();
	}

// ******************* end  get online   *******************
    Notification.error('Data in Process......');
	setTimeout(function(){
			console.log('realtimeUser')
			var user = firebase.database().ref('user');
			user.on('value',function(snapshots){
				if (snapshots.val() !== null && $scope.realtimeUser) {
					$scope.users = []
					,$scope.currentPage = 1
					,$scope.numPerPage = 10
					,$scope.maxSize = 5;


			    	var obj = [];
			    	var count = 1
				    snapshots.forEach(function (s){
						obj.push({
							count: 'No.' + count,
							key: s.key,
							address: s.val().address,
							displayName: s.val().displayName,
							email: s.val().email,
							lastAccess: s.val().lastAccess ? timeConverter(s.val().lastAccess) : '',
							created_at: s.val().created_at ? timeConverter(s.val().created_at) : '',
							phone: s.val().phone,
							uid: s.val().uid,
							status: s.val().status,
							photoURL: s.val().photoURL,
						});
						count++
					})

					$scope.todos = _.sortBy(obj, 'created_at').reverse();

					$scope.todosActive = _.filter($scope.todos, ['status',true])

					if (!$scope.$$phase) { $scope.$apply(); }

					$scope.$watch('currentPage + numPerPage', function() {
					    var begin = (($scope.currentPage - 1) * $scope.numPerPage)
					    , end = begin + $scope.numPerPage;
					    
					    $scope.users =  $scope.todos.slice(begin, end) 
					});

					Notification.success('Data is Success');
					
				}else{
					console.log('data null')
				}		
			})	
	},100)


	// activeCallback
	$scope.activeCallback = function(data) {
		var key = data.key;
		var dataActive = {
			status : data.status 
		}
		var user_active = firebase.database().ref('user/'+key);
		user_active.update(dataActive, function(err){
			if (!err) {
				Notification.success('Active Success !!!');
			} else {
				Notification.error('Active Error !!!');
			}
		})
	};

	// DeleteUser
	$scope.DeleteUser = function(data) {
		SweetAlert.swal({
		   title: "Are you sure?",
		   text: "Your will not be able to recover this record!",
		   type: "warning",
		   showCancelButton: true,
		   confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
		   cancelButtonText: "cancel",
		   closeOnConfirm: false}, 
		function(isConfirm){ 
		   if (isConfirm) {
			var userData = firebase.database().ref('user/'+data.key);
			userData.remove(function(err){
				if (err) {
					SweetAlert.swal("Error !!!");
				}else{
					Notification.success('Deleted  Success !!!');
	        		SweetAlert.swal("Deleted!", "Your record has been deleted.", "success");
	        		$state.reload()
				}
			});
		   }
		});
	};



	
	
}]);
