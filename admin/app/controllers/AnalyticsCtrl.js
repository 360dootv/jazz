app.controller('AnalyticsCtrl',['$scope','$rootScope', '$location','$state','SweetAlert','$stateParams','Notification',
	function ($scope,$rootScope,$location,$state,SweetAlert,$stateParams,Notification){
// *******************  variable   *******************  
	var settings = firebase.database().ref('settings');
	var event = firebase.database().ref('event');
	var views_live = firebase.database().ref('event/views_live');
	var userlogs = firebase.database().ref('userlogs');
	var playlists = firebase.database().ref('playlists');
	var views = firebase.database().ref('view/main');
// *******************  variable   *******************  
function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  	if(month.toString().length == 1) {
        var month = '0'+month;
    }
    if(date.toString().length == 1) {
        var date = '0'+date;
    }   
    if(hour.toString().length == 1) {
        var hour = '0'+hour;
    }
    if(min.toString().length == 1) {
        var min = '0'+min;
    }
    if(sec.toString().length == 1) {
        var sec = '0'+sec;
    }   
  var time = date + '-' + month + '-'+ year+' '+ hour + ':' + min + ':' + sec ;
  return time;
}


	views.on('value', function(snapshots){
		console.log(snapshots.val())
		if (snapshots.val() !== null) {
			$scope.views = []
			snapshots.forEach(function (s){
				var obj = {
					key: s.key,
				};
				$scope.views.push(obj)
			})
			Notification.success('Online User is Activities');
		}else{
			$scope.views = false
			$scope.zero = '0'
			Notification.error('Online User is nothing!!!');
		}
		if (!$scope.$$phase) { $scope.$apply(); }
	})
// *******************  get settings   *******************  	
	settings.on('value',function(snapshots){
		if (snapshots.val() !== null) {
			$scope.settings = snapshots.val()
			sessionStorage.setItem('episode', $scope.settings.episode)
			if (!$scope.$$phase) {
				$scope.$apply();
			}
			if (!$scope.$$phase) { $scope.$apply(); }
		}else{
			console.log('data null')
		}
	})
// ******************* end  get settings   *******************

// *******************  get online   *******************  	
	event.on('value',function(snapshots){
		if (snapshots.val() !== null) {
			$scope.events = snapshots.val()
		}else{
			console.log('data null')
		}
		if (!$scope.$$phase) { $scope.$apply(); }
	})
// ******************* end  get online   *******************

// *******************  get online   *******************  	
	views_live.on('value',function(snapshots){
		if (snapshots.val() !== null) {
			$scope.views = []
			snapshots.forEach(function (s){
				var obj = {
					key: s.key,
				};
				$scope.views.push(obj)
			})
			Notification.success('Online User is Activities');
		}else{
			$scope.views = false
			$scope.v = '0'
			Notification.error('Online User is nothing!!!');
			console.log('Online User is nothing!!!')
		}
		if (!$scope.$$phase) { $scope.$apply(); }
	})
// ******************* end  get online   *******************
    Notification.error('Data Analytics in Process......');
	setTimeout(function(){
		Notification.success('Data Analytics is Success');
		var userlogs = firebase.database().ref('userlogs');
		userlogs.once('value',function(snapshots){
			if (snapshots.val() !== null) {
				$scope.logs = snapshots.val()
				$scope.user_logs = [];	
				var count = 1;
				snapshots.forEach(function (s){
					$scope.user_logs.push([
						count, 
						s.val().objectID, 
						s.val().name, 
						s.val().email,
						s.val().ip, 
						s.val().country_name,
						s.val().city ? s.val().city : 'Unknown',
						s.val().device,
						s.val().os_version,
						s.val().browser,
						timeConverter(s.val().dateTime)
					]);
					count++;
				})

				var city = _($scope.logs).groupBy(item => item.city ? item.city : 'Unknown' ).value();
				var result_city = _.map(city, function (info) {
			        var val = {
			          city: info[0].city,
			          count: info.length++
			        }
			        return val
			    })
			    var device = _($scope.logs).groupBy(item => item.device ? item.device : 'Unknown' ).value();
				var result_device = _.map(device, function (info) {
			        var val = {
			          device: info[0].device,
			          os_version: info[0].os_version,
			          count: info.length++
			        }
			        return val
			    })
			    var browser = _($scope.logs).groupBy(item => item.browser ? item.browser : 'Unknown' ).value();
				var result_browser = _.map(browser, function (info) {
			        var val = {
			          browser: info[0].browser,
			          count: info.length++
			        }
			        return val
			    })
				
				$scope.$apply(function(){
					$scope.city = result_city
					$scope.devices = result_device
					$scope.browsers = result_browser	
				})

				$(document).ready(function(){
					$('#userlog_analytics').DataTable( {
				        data: $scope.user_logs.reverse(),
				        "bDestroy": true,
				        "ordering": false,
				        "autoWidth": true,
				        columns: [
				        	{ title: "No." },
				        	{ title: "ID" },
				        	{ title: "Name" },
				        	{ title: "Email" },
				            { title: "IP" },
				            { title: "Country" },
				            { title: "City" },
				            { title: "Device" },
				            { title: "Version" },
				            { title: "Browser" },
				            { title: "Last Access" },
				        ]
				    });
				});	
				if (!$scope.$$phase) { $scope.$apply(); }
				
			}else{
				console.log('data null')
			}		
		})
	},2000)

}]);
