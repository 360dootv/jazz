// Firebase -->
// ***********************************************************************************************************************
   var config = {
        apiKey: "AIzaSyCfSNSONM7XsijA-xWLB_jMosYSFpubXgo",
        authDomain: "jazz-c8ba1.firebaseapp.com",
        databaseURL: "https://jazz-c8ba1.firebaseio.com",
        projectId: "jazz-c8ba1",
        storageBucket: "jazz-c8ba1.appspot.com",
        messagingSenderId: "1074998454805"
    };
    firebase.initializeApp(config);
//   * TODO(DEVELOPER): Paste the initialization snippet from: Firebase Console > Overview > Add Firebase to your web app. *
//   *********************************************************************************************************************** 
var app = angular.module('app',['ui.router','ezfb']);

app.config(['$stateProvider', '$urlRouterProvider','$locationProvider','ezfbProvider',
function ($stateProvider, $urlRouterProvider, $locationProvider,ezfbProvider) {

    $urlRouterProvider.otherwise('/');
	$locationProvider.html5Mode(false).hashPrefix('');
    ezfbProvider.setInitParams({
        appId: '107766546460823'
    });
    $stateProvider
        .state('main', {
            url: '/',
            templateUrl: 'app/views/includes/index.html',
            controller: 'MainCtrl'
        })
        .state('cheateApp', {
            url: '/live',
            templateUrl: 'app/views/app/index.html',
            controller: 'AppCtrl'
        })

    // location.assign('https://www.honda.co.th/');
    
}])

app.run(['$rootScope','$http', function($rootScope,$http){
    // $http.get('http://project.thailivestream.com/analytics-service/').then(function(resp){
    //     if(resp.data.browser === "Line"){
    //         alert('กรุณาใช้ Web Browser อื่นในการเข้าใช้งาน (มุมขวาบนของมือถือ) ')
    //     }else{
    //         firebase.auth().onAuthStateChanged(function(user) {
    //           if (user) {
    //               var member = firebase.database().ref('user/' + user.uid );
    //               member.on('value', function (snapshot){
    //                   console.log('snapshot.val()',snapshot.val());
    //                   $rootScope.user = snapshot.val()
    //                   sessionStorage.setItem('user', JSON.stringify(snapshot.val()))
    //               })
    //           }else{
    //               sessionStorage.clear();
    //                console.log(false)
    //           }
    //         });
    //     }
    // })
   
}])