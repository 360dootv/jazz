app.controller('AppCtrl',['$scope','$state','$interval', 'ImgService' , '$http',
    function ($scope,$state,$interval,ImgService,$http){
    $scope.statusUser = false  
    $scope.activeUser = false    
    $scope.selectedTab = 4
    var settings = firebase.database().ref('settings');
    var comments = firebase.database().ref('comments');
    var commentsPin = firebase.database().ref('commentspin');
    settings.on('value', function(snapshot){
        $scope.setting = snapshot.val();
        if ($scope.setting.groupmenu.mainview.status === true) {
            setTimeout(function(){
                $('#live').attr('src', $scope.setting.groupmenu.mainview.url )
            },500)  
        } else {
            $scope.setting.groupmenu.mainview.url = ''
            $('#live').attr('src', '' )
        }

        if (!$scope.$$phase) { $scope.$apply(); }
    })

    comments.on('value', function(snapshot){
        $scope.comment = snapshot.val();
        if (!$scope.$$phase) { $scope.$apply(); }
    })

    commentsPin.on('value', function(snapshot){
        $scope.commentsPin = snapshot.val();
        if (!$scope.$$phase) { $scope.$apply(); }
    })
    
    var userData = firebase.database().ref('user');

    userData.orderByChild("status").equalTo(true).once('value', function(snapshot){
        $scope.profile = [];
        snapshot.forEach(function(info){
            $scope.profile.push(info.val())
        })
        sessionStorage.setItem('imgStore',JSON.stringify($scope.profile))
        function imageStore () {
            $scope.imageStore = ImgService.getSetting($scope.profile)
        }
        imageStore()
        $interval(function() {
            imageStore()
        },25000);
        if (!$scope.$$phase) { $scope.$apply(); }
    })

    $scope.select = function (data) {
        $scope.selectedTab = 1;
        setTimeout(function(){
            $('#live').attr('src', '' )
            $('#vod').attr('src', data )
        },500)  
    }
    $scope.selectApp = function (data) {
        $scope.selectedTab = data;
    }

    $scope.selectMain = function (data) {
        $scope.selectedTab = 4;
        $('#live').attr('src', data.mainview.url )
        if (!$scope.$$phase) { $scope.$apply(); }
    }

    // facebook
    $scope.signInFacebook  = function (data){
        var provider = new firebase.auth.FacebookAuthProvider();
        provider.addScope('user_likes');
        firebase.auth().signInWithRedirect(provider);
    }

    
    function initApp() {
        firebase.auth().getRedirectResult().then(function(result) {
            if (result.credential) {
                $scope.user = result.user.providerData[0]
                $scope.statusUser = true
                var member = firebase.database().ref('user/'+result.user.uid);
                member.once('value', function(snapshot){
                     if (snapshot.val() === null) {
                        $scope.activeUser = true    
                        $scope.user = {
                            uid: result.user.providerData[0].uid,
                            email: result.user.email,
                            displayName: result.user.displayName,
                            phone: result.user.phone,
                            photoURL: 'http://graph.facebook.com/'+result.user.providerData[0].uid+'/picture?type=normal'
                        }                        
                     }
                    if (!$scope.$$phase) { $scope.$apply(); }
                })           
            } else {
                return false
            }
        });
    }

    initApp()

    $scope.signInSystem = function (data){
        firebase.auth().onAuthStateChanged(function(user) {
          if (user) {
            var memberData = {
                uid: data.uid,
                address: data.address,
                email: data.email,
                displayName: data.displayName,
                phone: data.phone,
                photoURL: 'http://graph.facebook.com/'+data.uid+'/picture?type=normal',
                status: false,
                created_at: firebase.database.ServerValue.TIMESTAMP
            }
            var member = firebase.database().ref('user/' + user.uid );
            member.set(memberData, function (err){
                if(!err){
                    $scope.activeUser = false     
                }
            })
          } else {
            alert('ระบบมีปัญหา กรุณาติดต่อเจ้าหน้าที่')
          }
        });
    }



}])


