app.controller('MainCtrl',['$scope', '$state', '$interval','ImgService','ezfb','$http',
    function ($scope, $state, $interval, ImgService, ezfb, $http){
     $scope.statusUser = false
	$scope.user = null
    $scope.setting = null

    var settings = firebase.database().ref('settings');
    settings.on('value', function(snapshot){
        $scope.setting = snapshot.val();
        if ($scope.setting.startLive === true) {
            $state.go('cheateApp')
        }
        if (!$scope.$$phase) { $scope.$apply(); }
    })

    var allowUser = false
    // [signInFacebook]
	$scope.signInFacebook  = function (data){
        var provider = new firebase.auth.FacebookAuthProvider();
        provider.addScope('user_likes');
        firebase.auth().signInWithRedirect(provider);
    }

    
    function initApp(s) {
        firebase.auth().getRedirectResult().then(function(result) {
            if (result.credential) {
                $('.loginBtn--facebook').button('loading');
                var member = firebase.database().ref('user/'+result.user.uid);
                member.once('value', function(snapshot){
                     if (snapshot.val() !== null) {
                        if (!allowUser) {
                            $scope.statusUser = true
                            var memberUpdate = firebase.database().ref('user/' + result.user.uid );
                            memberUpdate.update({
                                uid: result.user.providerData[0].uid, 
                                photoURL: 'http://graph.facebook.com/'+result.user.providerData[0].uid+'/picture?type=normal'
                            })
                            if (!$scope.$$phase) { $scope.$apply(); }                            
                        } else {
                            $state.go('cheateApp')
                        }
                     } else {
                            $scope.user = {
                                uid: result.user.providerData[0].uid,
                                email: result.user.email,
                                displayName: result.user.displayName,
                                phone: result.user.phone,
                                photoURL: result.user.photoURL
                            }                        
                     }
                    if (!$scope.$$phase) { $scope.$apply(); }
                })           
            } else {
                return false
            }
        });
    }

    initApp($scope.setting)

    $scope.signInSystem = function (data){
        firebase.auth().onAuthStateChanged(function(user) {
          if (user) {
            var memberData = {
                            uid: data.uid,
                            address: data.address,
                            email: data.email,
                            displayName: data.displayName,
                            phone: data.phone,
                            photoURL: 'http://graph.facebook.com/'+data.uid+'/picture?type=normal',
                            status: false,
                            created_at: firebase.database.ServerValue.TIMESTAMP
                        }
            var member = firebase.database().ref('user/' + user.uid );
            member.set(memberData, function (err){
                if(!err){
                    if (allowUser) {
                        $state.go('cheateApp')
                    } else {
                        $scope.statusUser = true     
                    }
                }
            })
          } else {
            alert('ระบบมีปัญหา กรุณาติดต่อเจ้าหน้าที่')
          }
        });
    }



	$scope.signOut = function () {
        // [signOut]
		firebase.auth().signOut();
		$state.reload()
	}
    
    // parallax
    var scene = document.getElementById('scene');
    var parallax = new Parallax(scene);


    var userData = firebase.database().ref('user');
    userData.orderByChild("status").equalTo(true).once('value', function(snapshot){
        $scope.profile = [];
        snapshot.forEach(function(info){
            $scope.profile.push(info.val())
        })
        sessionStorage.setItem('imgStore',JSON.stringify($scope.profile))
        function imageStore () {
            $scope.imageStore = ImgService.getSetting($scope.profile)
        }
        imageStore()
        $interval(function() {
            imageStore()
        },25000);
        if (!$scope.$$phase) { $scope.$apply(); }
    })

}])
