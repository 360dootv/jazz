app.factory('ImgService', ['$http', function ($http) {
    return {
        getSetting : function(user){
            var array = [];
            var count = 1248;
            var concat = null
            // var userActive = _.filter(user, ['status', true]);
            var userActive = user
            if (userActive.length <= 1248) {
                count -= userActive.length;
                for (var i = 0; i < count; i++) {
                    var image = _.sample(['Human-9']);
                    array.push({
                        id: i,
                        photoURL: 'assets/images/human/'+ image + '.png'
                    })
                }
                concat = _.concat(userActive, array);
            } else {
                concat = userActive;
            }
            var dataSlice = _.shuffle(concat);
            return _.chunk(dataSlice.slice(0,1248), 78)
        }
    };
}]);
